# Rust Command-Line Tool with Testing

## Description

The Rust command-line tool is designed for efficient data ingestion and processing, specifically tailored to work with CSV files containing car data. It allows users to filter and query the dataset based on various criteria such as car name, year, or other attributes. The core functionality is built in Rust, leveraging its performance and safety features to handle data efficiently. Additionally, the project includes a comprehensive suite of unit tests, ensuring the correctness and reliability of the tool's functionality. These tests validate the data processing logic and filter criteria, confirming that the application behaves as expected under different scenarios.

## Demo

The sample tests are performed on a dataset named cardata.csv, which contains car-related information. This data has been specifically prepared and structured for this project, where the Rust functions are tailored to filter and query significant car attributes such as Car_Name, Year, Fuel_Type, and Selling_Price according to user input.

- **Command-Line Tool with Car Name**


If users want to query car information by the name of the car, they should use Car_Name as the search key, and specify the car name. The command-line tool would be invoked like this: `./target/debug/mini8 Car_Name "<car_name>"`

![cli_name](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG120.jpg)

- **Command-Line Tool with Car Year**


If users want to query car information by the year of manufacture, they can use Year as the search key, and then specify the year. The command-line tool would be used in the format: `./target/debug/mini8 Year "<year>"`

![cli_year](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG121.jpg)

- **Command-Line Tool with Unknown Input**

If the car or the year that the users wish to find was not incorporated in the dataset, the command-line tool simply returns *No cars found*:

![unknown](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG122.jpg)

- **Cargo Test**

Unit tests are integrated within `src/main.rs`. Calling `cargo test` will yield the following output:

![test](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG123.jpg)


## Steps

- `cargo new miniproect8`
- Add `csv`, `serde_json` and `serde` to `Cargo.toml`
- Build Rust functions in `src/main.rs`
    - Define the car structure
    - Write functions for reading CSV files and filtering cars by name or year
    - Implement command-line tool structures:
        ```rust
        let args: Vec<String> = env::args().collect();
        if args.len() < 3 {
            eprintln!("Usage: {} <field> <value>", args[0]);
            process::exit(1);
        }

        let field = &args[1];
        let value = &args[2];
        ```
    - Finish `fn main()` with car matching
    - Add unit tests:
        ```rust
        #[cfg(test)]
        mod tests {
            use super::*;

            #[test]
            fn test_filter_cars_by_name() {
                let cars = vec![
                    Car { Car_Name: "ciaz".to_string(), Year: 2017, Selling_Price: 7.25, Present_Price: 9.85, Kms_Driven: 6900, Fuel_Type: "Petrol".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
                    Car { Car_Name: "swift".to_string(), Year: 2014, Selling_Price: 4.60, Present_Price: 6.87, Kms_Driven: 42450, Fuel_Type: "Diesel".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
                ];

                let filtered_cars = filter_cars_by_field(&cars, "Car_Name", "ciaz");
                assert_eq!(filtered_cars.len(), 1);
                assert_eq!(filtered_cars[0].Car_Name, "ciaz");
            }

            #[test]
            fn test_filter_cars_by_year() {
                let cars = vec![
                    Car { Car_Name: "ciaz".to_string(), Year: 2017, Selling_Price: 7.25, Present_Price: 9.85, Kms_Driven: 6900, Fuel_Type: "Petrol".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
                    Car { Car_Name: "swift".to_string(), Year: 2014, Selling_Price: 4.60, Present_Price: 6.87, Kms_Driven: 42450, Fuel_Type: "Diesel".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
                ];

                let filtered_cars = filter_cars_by_field(&cars, "Year", "2017");
                assert_eq!(filtered_cars.len(), 1);
                assert_eq!(filtered_cars[0].Year, 2017);
            }

        } 
        ```
- `cargo build`
- Utilize command-line tool in the format defined above
- `cargo test`
