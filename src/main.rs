use serde::Deserialize;
use std::env;
use std::fs::File;
use std::process;
use csv::ReaderBuilder;

#[derive(Debug, Deserialize, PartialEq, Clone)]
struct Car {
    Car_Name: String,
    Year: u32,
    Selling_Price: f64,
    Present_Price: f64,
    Kms_Driven: u32,
    Fuel_Type: String,
    Seller_Type: String,
    Transmission: String,
    Owner: u32,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        eprintln!("Usage: {} <field> <value>", args[0]);
        process::exit(1);
    }

    let field = &args[1];
    let value = &args[2];

    match read_cars_from_csv("cardata.csv") {
        Ok(cars) => {
            let filtered_cars = filter_cars_by_field(&cars, field, value);
            if filtered_cars.is_empty() {
                println!("No cars found with {} = {}", field, value);
            } else {
                for car in filtered_cars {
                    println!("{:?}", car);
                }
            }
        },
        Err(e) => {
            eprintln!("Error reading cars: {}", e);
            process::exit(1);
        },
    }
}

fn read_cars_from_csv(file_path: &str) -> Result<Vec<Car>, csv::Error> {
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    rdr.deserialize().collect()
}

fn filter_cars_by_field(cars: &[Car], field: &str, value: &str) -> Vec<Car> {
    cars.iter().filter(|car| {
        match field {
            "Car_Name" => car.Car_Name.to_lowercase() == value.to_lowercase(),
            "Year" => car.Year.to_string() == value,
            // Add more fields as needed for filtering
            _ => false,
        }
    }).cloned().collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filter_cars_by_name() {
        let cars = vec![
            Car { Car_Name: "ciaz".to_string(), Year: 2017, Selling_Price: 7.25, Present_Price: 9.85, Kms_Driven: 6900, Fuel_Type: "Petrol".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
            Car { Car_Name: "swift".to_string(), Year: 2014, Selling_Price: 4.60, Present_Price: 6.87, Kms_Driven: 42450, Fuel_Type: "Diesel".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
        ];

        let filtered_cars = filter_cars_by_field(&cars, "Car_Name", "ciaz");
        assert_eq!(filtered_cars.len(), 1);
        assert_eq!(filtered_cars[0].Car_Name, "ciaz");
    }

    #[test]
    fn test_filter_cars_by_year() {
        let cars = vec![
            Car { Car_Name: "ciaz".to_string(), Year: 2017, Selling_Price: 7.25, Present_Price: 9.85, Kms_Driven: 6900, Fuel_Type: "Petrol".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
            Car { Car_Name: "swift".to_string(), Year: 2014, Selling_Price: 4.60, Present_Price: 6.87, Kms_Driven: 42450, Fuel_Type: "Diesel".to_string(), Seller_Type: "Dealer".to_string(), Transmission: "Manual".to_string(), Owner: 0 },
        ];

        let filtered_cars = filter_cars_by_field(&cars, "Year", "2017");
        assert_eq!(filtered_cars.len(), 1);
        assert_eq!(filtered_cars[0].Year, 2017);
    }

}

